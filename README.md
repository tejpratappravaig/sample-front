# check version:-

sudo node -v
sudo npm -v

# update node:- 

sudo npm cache clean -f <br/>
sudo npm install -g n <br/>
sudo n stable

# update npm:-

sudo npm install npm -g

# install dependencies:-

sudo npm install

# run project:-

sudo npm start <br/>
(runs on localhost:3000)

import React, { useState,useEffect } from 'react'
import axios from 'axios';

const Vehicle = () => {

    const [speed,setSpeed] = useState(55);
    const [status, setStatus] = useState('P');
    const [odometer,setOdometer] = useState(67);
    const [drivetime,setDrivetime] = useState(435677);
    const [ac,setAc] = useState(0);
    const [display,setdisplay] = useState(0);

    useEffect(()=>{
        console.log(ac,'ac');
        let stats;
        if(ac===0){
            stats='OFF';
        }
        else{
            stats='ON';
        }
        // Using Get Request for Temp solution ,,, Will have to change in futue !!!important
        axios.get('http://127.0.0.1:8000/GetChn/AC/'+stats)
        .then(response  => {
            console.log("response received",response);
        })
        .catch(function (error) {
        console.log(error);
        })


        // const temp = {status:stats};
        // axios.post('http://127.0.0.1:8000/Chn',{
        //     temp
        // }).then(res=>{
        //     console.log(res);
        // })
        // .catch(err=>{
        //     console.log(err);
        // })
    },[ac])

    useEffect(() => {
        setInterval(() => {
            axios.get('http://127.0.0.1:8000/dict')
            .then(response  => {
                console.log("response received",response);
                // setdisplay(response.ac);
            })
              .catch(function (error) {
                console.log(error);
              })
        }, 155000);
      });



    let time={};

    if(drivetime>0){
        time = {
            hours: Math.floor((drivetime / (1000 * 60 * 60)) % 24),
            minutes: Math.floor((drivetime / 1000 / 60) % 60),
            seconds: Math.floor((drivetime / 1000) % 60)
          };
    }

    return (
        <div style={{textAlign:"center"}}>
        <h1>Speed : {speed} km/h</h1>
        <h1>Car Status : {status} </h1>
        <h1>Odometer : {odometer}</h1>
        <h1>Drive Time : {time.hours}hrs {time.minutes}mins {time.seconds}secs</h1>
        <h1>Air Condition <button onClick={()=>{setAc(1-ac)}}>On/Off</button>  {display}</h1>
        </div>
    )
}

export default Vehicle

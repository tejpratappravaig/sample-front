import React,{useState} from 'react'
import Slider from 'rc-slider';

const Data=()=>{

    const [rpm,setRpm] = useState(55);
    const [linevoltage,setLineVoltage] = useState(100.4356546);
    const [linecurrent,setLineCurrent] = useState(140.53456436);
    const [expectedRange, setExpetedRange] = useState(55.7);
    const [motorTemp, setMotorTemp] = useState(77);
    const [invertorTemp, setInvertorTemp] = useState(67);
    const [regen, setRegen] = useState(53)

    const [regenSlider,setRegenSlider] = useState(0); //
    const [temp,setTemp] = useState([5,3,6,2,6,6,3,1,6,7]);

    const [cellTemp, setcellTemp] = useState([5,3,6,2,6,6,3,1,6,7,5,3,6,2,6,6,3,1,6,7,5,3,6,2,6,6,3,1,6,7,5,3,6,2,6,6,3,1,6,7,5,3,6,2,6,6,3,1,6,7,5,3,6,2,6,6,3,1,6,7]);

    cellTemp.sort((a,b)=> b-a);


    const makeGrid = (arr)=> {
           return( <div className="row container">
                {arr.map((element,id) => {
                if(id<5){
                    return (<div className="col-sm-2" key={id} style={{backgroundColor:"red",border:"1px solid red"}}>
                <div><strong>{element}</strong></div></div>);
                }
                else if(id>54){
                    return(<div className="col-sm-2" key={id} style={{backgroundColor:"blue",border:"1px solid red"}}>
                <div><strong>{element}</strong></div></div>);
                }
                else{
                    return(<div className="col-sm-2" key={id} style={{backgroundColor:"white",border:"1px solid red"}}>
                <div><strong>{element}</strong></div></div>);
                }
            })}
            </div>
           )
    }

    const calculateAvg = (arr)=>{
        var total=0;
        var count=0;
        arr.forEach((ele)=>{
            total+=ele;
            count++;
        })
        return total/count;
    }

    let batteryTemp = {};

    if(temp.length){
        batteryTemp = {
            minimum : Math.min(...temp),
            maximum : Math.max(...temp),
            average : calculateAvg(temp)
        }
    }


    return (
        <div style={{textAlign:"center"}}>
            <h1>Motor RPM : {rpm} rpm</h1>
            <h1>Line Voltage : {linevoltage.toFixed(2)} V</h1>
            <h1>Line Current : {linecurrent.toFixed(2)} A</h1>
            <h1>Expected Range : {expectedRange} Watt/km</h1>
            <h1>Motor Temperature : {motorTemp}<sup>*</sup>C</h1>
            <h1>Invertor Temperature : {invertorTemp}<sup>*</sup>C</h1>
            <h1>Regen : {regen}%</h1>
            <div style={{fontSize:"28px"}}>
            <label for="regen">Regen Slider</label>
                <input type="range" id="regen" min="0" max="100" value={regenSlider} onChange={(e)=>setRegenSlider(e.target.value)} style={{ verticalAlign:"middle"}}/>
                <span>{regenSlider}</span>
            </div>
            <h1>400V Battery temperature </h1>
            <ul>
                <h2>Maximum : {batteryTemp.maximum}</h2>
                <h2>Minimum : {batteryTemp.minimum}</h2>
                <h2>Average : {batteryTemp.average}</h2>
            </ul>
            <div style={{ width:"100%", marginBottom:"20px"}}>
            {makeGrid(cellTemp)}
            </div>
            
        </div>
    )
}

export default Data

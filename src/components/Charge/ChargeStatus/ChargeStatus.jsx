import React from 'react'

import './ChargeStatus.css';

const  ChargeStatus=(props) => {
    let component = "Not Charging";
    if(props.status===1){
        component = <span>Yes    <div className="box" style={{display:"inline-block"}}></div></span>;
    }

    return (
        <>
        {component}
        </>
    )
}

export default ChargeStatus

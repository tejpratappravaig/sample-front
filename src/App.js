import './App.css';
import Charging from './components/Charge/Charging';
import Vehicle from './components/Vehicle/Vehicle';
import Data from './components/Vehicle/Data';

import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";

const App=()=> {

  return (
    <Router>
       <div>
        <nav>
          <ul>
            <li>
              <Link to="/">Charge</Link>
            </li>
            <li>
              <Link to="/vehicle">Vehicle</Link>
            </li>
            <li>
              <Link to="/data">Data/ Diagnosis</Link>
            </li>
          </ul>
        </nav>
        <Switch>
          <Route path="/vehicle">
            <Vehicle/>
          </Route>
          <Route path="/data">
            <Data/>
          </Route>
          <Route path="/">
          <Charging/>
          </Route>
        </Switch>
      </div>
    </Router>
  );
}

export default App;



